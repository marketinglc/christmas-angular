import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  audio: HTMLAudioElement;

  ngOnInit(): void {
    this.audio  = new Audio();
    this.audio.src = '/assets/sounds/Song.mp3';
    this.audio.load();
    this.audio.loop = true;
    this.audio.play();
  }

}
